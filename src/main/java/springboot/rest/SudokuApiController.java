package springboot.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.dto.SudokuErrorDto;
import springboot.model.SudokuCheckService;
import springboot.model.SudokuRepository;

@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class SudokuApiController {

    private SudokuCheckService sudokuCheckService;

    public SudokuApiController() {

    }

    @PostMapping("/sudoku/verify")
    public ResponseEntity<SudokuErrorDto> postSudoku() {

        sudokuCheckService = new SudokuCheckService();
        if (sudokuCheckService.check()) {
            SudokuRepository sudokuRepository = new SudokuRepository();
            return ResponseEntity.ok().body(null);
        } else {
            return ResponseEntity.badRequest().body(sudokuCheckService.getSudokuErrorDto());
        }

    }

}
