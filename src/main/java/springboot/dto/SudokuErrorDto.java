package springboot.dto;

public class SudokuErrorDto {

    private String lineIds;
    private String kolumnIds;
    private String areaIds;

    public SudokuErrorDto(String lineIds, String kolumnIds, String areaIds) {
        this.lineIds = lineIds;
        this.kolumnIds = kolumnIds;
        this.areaIds = areaIds;
    }

    public SudokuErrorDto() {
    }

    public String getLineIds() {
        return lineIds;
    }

    public String getKolumnIds() {
        return kolumnIds;
    }

    public String getAreaIds() {
        return areaIds;
    }
}
