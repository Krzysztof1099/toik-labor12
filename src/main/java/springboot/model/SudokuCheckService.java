package springboot.model;

import org.springframework.beans.factory.annotation.Autowired;
import springboot.dto.SudokuErrorDto;

public class SudokuCheckService {

    private SudokuErrorDto sudokuErrorDto;
    @Autowired
    private SudokuRepository sudokuRepository;

    public SudokuErrorDto getSudokuErrorDto() {
        return sudokuErrorDto;
    }

    public SudokuCheckService() {
    }

    public boolean check() {

        sudokuRepository = new SudokuRepository();
        int[][] sudokuTable = sudokuRepository.getSudokuTable();


        String lineIds = "";
        String kolumnIds = "";
        String areaIds = "";


        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 8; col++) {
                for (int col2 = col + 1; col2 < 9; col2++) {
                    if (sudokuTable[row][col] == sudokuTable[row][col2]) {
                        lineIds += Integer.toString(row + 1) + ',';
                    }
                }
            }
        }


        for (int col = 0; col < 9; col++) {
            for (int row = 0; row < 8; row++) {
                for (int row2 = row + 1; row2 < 9; row2++) {
                    if (sudokuTable[row][col] == sudokuTable[row2][col]) {
                        kolumnIds += Integer.toString(col + 1) + ',';
                    }
                }
            }
        }


        for (int row = 0; row < 9; row += 3) {
            for (int col = 0; col < 9; col += 3) {

                for (int pos = 0; pos < 8; pos++) {
                    for (int pos2 = pos + 1; pos2 < 9; pos2++) {
                        if (sudokuTable[row + pos % 3][col + pos / 3] == sudokuTable[row + pos2 % 3][col + pos2 / 3]) {
                            areaIds += '(' + Integer.toString(row / 2) + ',' + Integer.toString(col / 2) + ')' + ',';
                        }
                    }
                }
            }
        }
        lineIds = cutLastChar(lineIds);
        kolumnIds = cutLastChar(kolumnIds);
        areaIds = cutLastChar(areaIds);

        sudokuErrorDto = new SudokuErrorDto(lineIds, kolumnIds, areaIds);
        if (SudokuError()) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean SudokuError() {
        if (!sudokuErrorDto.getLineIds().isEmpty() || !sudokuErrorDto.getKolumnIds().isEmpty() || !sudokuErrorDto.getAreaIds().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private String cutLastChar(String toCut) {
        if (toCut != null && toCut.length() > 0 && toCut.charAt(toCut.length() - 1) == ',') {
            toCut = toCut.substring(0, toCut.length() - 1);
        }
        return toCut;
    }
}
