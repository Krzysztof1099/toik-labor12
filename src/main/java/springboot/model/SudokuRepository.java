package springboot.model;

public class SudokuRepository {

    private int sudokuTable[][];

    public SudokuRepository() {
        this.sudokuTable = new SudokuReadFile().readCSV();
    }

    public int[][] getSudokuTable() {
        return sudokuTable;
    }

}
